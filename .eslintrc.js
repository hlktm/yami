module.exports = {
  // 设置我们的运行环境为浏览器 + es2021 + node ,否则eslint在遇到 Promise，window等全局对象时会报错
  env: {
    browser: true,
    es2021: true,
    node: true,
    // 开启setup语法糖环境
    'vue/setup-compiler-macros': true,
  },
  // 继承eslint推荐的规则集，vue基本的规则集，typescript的规则集
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
    'plugin:@typescript-eslint/recommended',
    'standard',
    'plugin:prettier/recommended', // 新增，必须放在最后面
  ],
  // 新增，解析vue文件
  parser: 'vue-eslint-parser',
  // 支持ts的最新语法
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  // 添加vue和@typescript-eslint插件，增强eslint的能力
  plugins: ['vue', 'eslint-plugin-prettier'],
  rules: {
    semi: 'off',
    'comma-dangle': 'off',
    // 关闭名称校验
    // 添加组件命名忽略规则
    'vue/multi-word-component-names': 'off',
    'prettier/prettier': 'warn',
    // // 在生产环境下  不能出现打印，弹框，打断点
    // 'no-console': process.env.NODE_ENV === 'development' ? 0 : 1,
    // 'no-alert': process.env.NODE_ENV === 'development' ? 0 : 1,
    // 'no-debugger': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-unused-vars': ['warn', { vars: 'all', args: 'none' }], // 没有使用的声明
    '@typescript-eslint/no-var-requires': 0,
    'space-before-function-paren': 0, // 在函数前有空格
    '@typescript-eslint/no-explicit-any': 'off',
  },
};
