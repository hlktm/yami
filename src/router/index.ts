import { createRouter, createWebHistory } from 'vue-router';
import type { RouteRecordRaw } from 'vue-router'; // 仅仅导入被用于类型注解或声明的声明语句，它总是会被完全删除，
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/home',
    name: '首页',
    component: () => import('../views/home/index.vue'),
    children: [
      {
        path: '/home',
        redirect: '/homepage',
        component: () => import('../views/productManagement/ProdProdTag.vue'),
      },
      {
        path: '/homepage',
        name: '首页',
        component: () => import('../views/homePage/index.vue'),
      },
      {
        path: '/prod-prodTag',
        name: '分组管理',
        component: () => import('../views/productManagement/ProdProdTag.vue'),
      },
      {
        path: '/prod-prodList',
        name: '产品管理',
        component: () => import('../views/productManagement/ProdProdList.vue'),
      },
      {
        path: '/prod-category',
        name: '分类管理',
        component: () => import('../views/productManagement/ProdCategory.vue'),
      },
      {
        path: '/prod-prodComm',
        name: '评论管理',
        component: () => import('../views/productManagement/ProdProdComm.vue'),
      },
      {
        path: '/prod-spec',
        name: '规格管理',
        component: () => import('../views/productManagement/ProdSpec.vue'),
      },
      {
        path: '/shop-notice',
        name: '公告管理',
        component: () => import('../views/StoreManagement/ShopNotice.vue'),
      },
      {
        path: '/shop-hotSearch',
        name: '热搜管理',
        component: () => import('../views/StoreManagement/ShopHotSearch.vue'),
      },
      {
        path: '/admin-indexImg',
        name: '轮播图管理',
        component: () => import('../views/StoreManagement/AdminIndexImg.vue'),
      },
      {
        path: '/shop-transport',
        name: '运费模板',
        component: () => import('../views/StoreManagement/ShopTransport.vue'),
      },
      {
        path: '/shop-pickAddr',
        name: '自提点管理',
        component: () => import('../views/StoreManagement/ShopPickAddr.vue'),
      },
      {
        path: '/user-user',
        name: '会员管理',
        component: () => import('../views/MemberManagement/UserUser.vue'),
      },
      {
        path: '/order-order',
        name: '订单管理',
        component: () => import('../views/OrderManagement/OrderOrder.vue'),
      },
      {
        path: '/sys-area',
        name: '地址管理',
        component: () => import('../views/SystemManagement/SysArea.vue'),
      },
      {
        path: '/sys-user',
        name: '管理员列表',
        component: () => import('../views/SystemManagement/SysUser.vue'),
      },
      {
        path: '/sys-role',
        name: '角色管理',
        component: () => import('../views/SystemManagement/SysRole.vue'),
      },
      {
        path: '/sys-menu',
        name: '菜单管理',
        component: () => import('../views/SystemManagement/SysMenu.vue'),
      },
      {
        path: '/sys-schedule',
        name: '定时任务',
        component: () => import('../views/SystemManagement/SysSchedule.vue'),
      },
      {
        path: '/sys-config',
        name: '参数管理',
        component: () => import('../views/SystemManagement/SysConfig.vue'),
      },
      {
        path: '/sys-log',
        name: '系统日志',
        component: () => import('../views/SystemManagement/SysLog.vue'),
      },
    ],
  },
  {
    path: '/login',
    name: '登录',
    component: () => import('../views/login/index.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(), // 路由模式
  routes,
});

export default router;
