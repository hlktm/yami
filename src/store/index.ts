// store.ts
import { createStore, useStore as baseUseStore, Store } from 'vuex';
import { InjectionKey } from 'vue';
import LoginStore from './modules/login';
import UserStore from './modules/home';
import { State } from './type';

// 定义 injection key
export const key: InjectionKey<Store<State>> = Symbol('');

// 创建一个新的 store 实例
export const store = createStore({
  state() {
    return {
      count: 0,
    };
  },
  mutations: {
    increment(state) {
      state.count++;
    },
  },
  modules: {
    login: LoginStore,
    user: UserStore,
  },
});
// 定义自己的 `useStore` 组合式函数
export function useStore() {
  return baseUseStore(key);
}
