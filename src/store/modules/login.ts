import { Module } from 'vuex';
import { login, userInfo } from '@/api/login/login';
import cache from '@/utils/localcache/cache';
import { moduleState } from '../type';

const LoginStore: Module<moduleState, any> = {
  namespaced: true,
  // 模块内容（module assets）
  state: () => ({
    // 模块内的状态已经是嵌套的了，使用 `namespaced` 属性不会对其产生影响
    token: '',
  }),
  getters: {
    // isAdmin () { ... } // -> getters['account/isAdmin']
  },
  actions: {
    // 获取token
    async getLogin({ commit }, payload) {
      const res = await login(payload);
      commit('changeToken', {
        key: 'token',
        data: res.data.access_token,
      });
      //   存储本地
      cache.setCache('access_token', res.data.access_token);
      cache.setCache('authorities', res.data.authorities);
      cache.setCache('expires_in', res.data.expires_in);
      cache.setCache('refresh_token', res.data.refresh_token);
      cache.setCache('shopId', res.data.shopId);
      cache.setCache('token_type', res.data.token_type);
      cache.setCache('userId', res.data.userId);
    },
  },
  mutations: {
    changeToken(state, payload) {
      state[payload.key] = payload.data;
    },
    // login () { ... } // -> commit('account/login')
  },
};

export default LoginStore;
