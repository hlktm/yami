import { Module } from 'vuex';
import { userInfo, getNav } from '@/api/home/home';
import cache from '@/utils/localcache/cache';
import { moduleState } from '../type';
const UserStore: Module<moduleState, any> = {
  namespaced: true,
  // 模块内容（module assets）
  state: () => ({
    // 模块内的状态已经是嵌套的了，使用 `namespaced` 属性不会对其产生影响
    nav: cache.getCache('nav') || '',
    userInfo: cache.getCache('userInfo') || '',
  }),
  getters: {
    // isAdmin () { ... } // -> getters['account/isAdmin']
  },
  actions: {
    async getUser({ commit }, payload) {
      const res = await userInfo(payload);
      cache.setCache('userInfo', res.data);
      commit('changeUser', {
        key: 'userInfo',
        data: res.data,
      });
    },
    async getNav({ commit }, payload) {
      const res = await getNav(payload);
      // console.log(res,"getNav")
      cache.setCache('nav', res.data);
      commit('changeNav', {
        key: 'nav',
        data: res.data,
      });
    },

    // login () { ... } // -> dispatch('account/login')
  },
  mutations: {
    changeUser(state, payload) {
      state[payload.key] = payload.data;
    },
    changeNav(state, payload) {
      state[payload.key] = payload.data;
    },
    // login () { ... } // -> commit('account/login')
  },
};

export default UserStore;
