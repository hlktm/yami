export interface responseData<T> {
  status: number;
  statusText: string;
  data: T;
}
export interface LoginData {
  access_token: string;
  authorities: any[];
  expires_in: number;
  refresh_token: string;
  shopId: number;
  token_type: string;
  userId: number;
}

// 登录接口的请求参数类型
export interface paramsType {
  credentials: string; // 密码
  imageCode: string; // 验证码
  principal: string; // 账号
  sessionUUID: string; // code截取吗
  t: number; // 时间戳
}

// 登录接口的响应data数据类型
interface statusDataType {
  access_token: string;
  authorities: any[];
  expires_in: number;
  refresh_token: string;
  shopId: number;
  token_type: string;
  userId: number;
}

// 登录接口的响应数据类型
export interface statusType {
  status: number;
  statusText: string;
  data: statusDataType;
}
