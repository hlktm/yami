import httpRequest from '@/utils/index';
import { responseData, LoginData } from './type';
import { UserData } from '../home/type';
export const login = (params: any) =>
  httpRequest.post<responseData<LoginData>>({
    url: '/login?grant_type=admin',
    data: params,
  });

export const userInfo = (params: any) =>
  httpRequest.get<responseData<UserData>>({
    url: '/sys/user/info',
    params,
  });
