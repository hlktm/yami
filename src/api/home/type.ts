export interface responseData<T> {
  status: number;
  statusText: string;
  data: T;
}
export interface UserData {
  createTime: string;
  email: string;
  mobile: string;
  roleIdList: number[];
  shopId: number;
  status: number;
  userId: number;
  username: string;
}

export interface NavData {
  authorities: any[];
  menuList: any[];
}
