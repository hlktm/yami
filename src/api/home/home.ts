import httpRequest from '@/utils/index';
import { responseData, UserData, NavData } from './type';
export const userInfo = (params: any) =>
  httpRequest.get<responseData<UserData>>({
    url: '/sys/user/info',
    params,
  });

export const getNav = (params: any) =>
  httpRequest.get<responseData<NavData>>({
    url: '/sys/menu/nav',
    params,
  });
