import { createApp } from 'vue';
import App from './App.vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import router from './router/index';
import { store, key } from './store/index';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import UUID from 'vue-uuid';
const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(router).use(store, key).use(UUID).use(ElementPlus).mount('#app');
// use(options)  如果options是一个对象的话，里面必须有一个install的方法。如果是函数的话，这个函数会被作Install方法来使用
