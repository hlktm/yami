import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
import { ElMessage, ElLoading } from 'element-plus';
// import "element/plus/theme-chalk/el-message.css";
import cache from '@/utils/localcache/cache';
import { LoadingInstance } from 'element-plus/lib/components/loading/src/loading';
const DEFAULT_LOADING = true;
interface httpRequestConfig extends AxiosRequestConfig {
  isShowLoading?: boolean;
}
class HttpRequest {
  instance: AxiosInstance;
  isShowLoading: boolean;
  isLoading?: LoadingInstance;
  constructor(config: httpRequestConfig) {
    this.instance = axios.create(config); // 创建了一个axios的实例
    this.isShowLoading = config.isShowLoading || DEFAULT_LOADING;
    // 添加请求拦截器
    this.instance.interceptors.request.use(
      (config) => {
        // console.log(
        //   localStorage.getItem('refresh_token'),
        //   "localStorage.getItem('refresh_token')"
        // );

        // 在发送请求之前做些什么
        if (this.isShowLoading) {
          // 发送请求之前的加载样式
          this.isLoading = ElLoading.service({
            lock: true,
            text: '正在发送请求.....',
            background: 'rgba(0,0,0,0.5)',
          });
        }
        this.isShowLoading = false;
        // console.log(cache.getCache('access_token'),44);

        const token = cache.getCache('access_token');
        const tokenType = cache.getCache('token_type');
        // config.headers['authorization']=localStorage.getItem('refresh_token');
        // console.log(token, 'token');
        // console.log(tokenType, 'tokenType');

        return {
          ...config,
          headers: {
            ...config.headers,
            Authorization: `bearer${token}`,
          },
        };
      },
      (error) => {
        // 对请求错误做些什么
        return Promise.reject(error);
      }
    );

    // 添加响应拦截器服务器响应之后 关闭loading
    this.instance.interceptors.response.use(
      (response) => {
        // 关闭loading
        this.isLoading?.close();
        console.log(response, 'response');

        return response;
      },
      (error) => {
        console.log(error, 'error');
        // 关闭loading
        this.isLoading?.close();

        if (error.response.status === 400) {
          ElMessage.error(error.response.data || '请求失败');
        } else if (error.response.status === 401) {
          ElMessage.error(error.response.data || '没有登录');
        }
        return Promise.reject(error);
      }
    );
  }

  request<T>(config: AxiosRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      this.instance
        .request<any, T>(config)
        .then((res) => {
          // 防止下一个请求受到影响
          this.isShowLoading = DEFAULT_LOADING;
          resolve(res);
        })
        .catch((err) => {
          this.isShowLoading = DEFAULT_LOADING;
          reject(err);
          return err;
        });
    });
  }

  get<T>(config: AxiosRequestConfig): Promise<T> {
    return this.request<T>({ ...config, method: 'GET' });
  }

  post<T>(config: AxiosRequestConfig): Promise<T> {
    return this.request({ ...config, method: 'POST' });
  }

  put<T>(config: AxiosRequestConfig): Promise<T> {
    return this.request({ ...config, method: 'PUT' });
  }

  delete<T>(config: AxiosRequestConfig): Promise<T> {
    return this.request({ ...config, method: 'DELETE' });
  }
}
export default HttpRequest;
