import HttpRequest from './request/httpRequest';
// console.log(import.meta.env.VITE_BASE_URL, 'import.meta.env.VITE_BASE_URL');
const httpRequest = new HttpRequest({
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 1000,
});
export default httpRequest;
